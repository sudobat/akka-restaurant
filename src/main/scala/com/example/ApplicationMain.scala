package com.example

import akka.actor.ActorSystem

object ApplicationMain extends App {
  val system = ActorSystem("RestaurantActorSystem")
  
  val customer1 = system.actorOf(CustomerActor.props, "customerActor1")
  val customer2 = system.actorOf(CustomerActor.props, "customerActor2")
  val customer3 = system.actorOf(CustomerActor.props, "customerActor3")
  val customer4 = system.actorOf(CustomerActor.props, "customerActor4")
  val customer5 = system.actorOf(CustomerActor.props, "customerActor5")
  
  customer1 ! CustomerActor.EnterRestaurant
  customer2 ! CustomerActor.EnterRestaurant
  customer3 ! CustomerActor.EnterRestaurant
  customer4 ! CustomerActor.EnterRestaurant
  customer5 ! CustomerActor.EnterRestaurant
  
  system.awaitTermination()
}