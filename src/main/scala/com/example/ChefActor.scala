package com.example

import akka.actor._

class ChefActor extends Actor with ActorLogging {
  import ChefActor._
  
  def receive = {
    case CookPlate(customer, plateId) =>
      log.info("ChefActor: CookPlate command received with id " + plateId)
      
      cookPlate(plateId)
      
      sender() ! WaiterActor.PlateCooked(customer, plateId)
  }
  
  def cookPlate(plateId: Int) : Unit = {
      log.info("ChefActor: Cooking...")
  }
}

object ChefActor {
  val props = Props[ChefActor]
  case class CookPlate(customer: ActorRef, plateId: Int)
}