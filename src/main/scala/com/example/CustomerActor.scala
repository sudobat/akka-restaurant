package com.example

import akka.actor.{Actor, ActorLogging, Props}

import java.util.Random
//import scala.collection.immutable.List

class CustomerActor extends Actor with ActorLogging {
  import CustomerActor._
  
  var counter = 0
  val waiter = context.actorOf(WaiterActor.props, "waiterActor")

  def receive = {
    case EnterRestaurant =>
      log.info(self.path.name + ": Entering the restaurant")
      waiter ! WaiterActor.WantToEat
      
    case Card(menu) =>
      log.info(self.path.name + ": Card recieved.")
      
      for ((plateId, plateName) <- menu)
        log.info(self.path.name + ": Menu: Plate " + plateId + ": " + plateName)
      
      val choosenPlateId = chooseRandomPlate(menu)
   
      waiter ! WaiterActor.PlateChoosen(choosenPlateId)
    
  	case PlateIsServed(plateId) =>
	  log.info(self.path.name + ": Plate " + plateId + " is served.")
	  //context.system.shutdown()
  }
  
  def chooseRandomPlate(menu: List[(Int, String)]): Int = {
    val rand = new Random(System.currentTimeMillis())
    val randomIndex = rand.nextInt(menu.length)
    
    val plateId = menu(randomIndex)._1
    
    return plateId
  }
}

object CustomerActor {
  val props = Props[CustomerActor]
  case object FoodIsServed
  case object EnterRestaurant
  case class Card(menu: List[(Int, String)])
  case class PingMessage(text: String)
  case class PlateIsServed(plateId: Int)
}