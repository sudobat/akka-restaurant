package com.example

import akka.actor._

class WaiterActor extends Actor with ActorLogging {
  import WaiterActor._
  
  val chef = context.actorOf(ChefActor.props, "chefActor")
  
  def receive = {
    case WantToEat =>
      log.info("WaiterActor: Customer wants to eat.")
      sender() ! CustomerActor.Card(List((1, "Patates"), (2, "Pomes")))
      
    case PlateChoosen(plateId) =>
      log.info("WaiterActor: Recieved command for plate " + plateId)
      
      chef ! ChefActor.CookPlate(sender, plateId)
    
    case PlateCooked(customer, plateId) =>
      log.info("WaiterActor: Received plate " + plateId + " from Chef")
      
      customer ! CustomerActor.PlateIsServed(plateId)
  }
}

object WaiterActor {
  val props = Props[WaiterActor]
  case object WantToEat
  case class PlateChoosen(plateId: Int)
  case class PlateCooked(customer: ActorRef, plateId: Int)
}